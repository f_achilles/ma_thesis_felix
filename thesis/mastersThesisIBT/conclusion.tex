%% conclusion.tex
%%

%% ==================
\chapter{Conclusion}
\label{ch:Conclusion}
%% ==================

In order to interpret the data collected in the experiments, it is necessary to look at the goals that were to be achieved by the fusion process.
Those were stated as
\begin{itemize}
\item creation of a sensor fusion algorithm between multiple modalities suited for endoscope tracking,
\item selection of devices suited for sub-millimeter accuracy,
\item development of new tracking paradigms that allow a higher robustness to occlusion, distortion, failure of single sensors or temporarily dropped sensor readings.
\end{itemize}
So one after the other will be reviewed.

\paragraph{Creation of a sensor fusion algorithm between multiple modalities suited for endoscope tracking}
The master filter that was described in section~\ref{subsec:masterFilter}, computes a weighted mean of multiple state vectors.
Those weights are hereby computed dynamically, based on the current measurement noise and system noise of each modality.
They are the state covariance matrices that are steadily updated by the Kalman Filter.
As such, their values are also depending on the measurement matrix $H$ and the transition matrix $A$, see equations~\eqref{eqn:KalmanPred} and~\eqref{eqn:KalmanUpdate}, but those are not likely to change during one recording.
Also the system noise matrix $Q$ should remain constant in the case of endoscope tracking, as it can be seen as the sudden unexpected change of the state vector, as if the position would suddenly start to vibrate, which is not very probable.
But certainly the measurement accuracy will vary during a recording, as it is affected by many independent effects.
Those were mentioned earlier as a partial occlusion of an optical marker, the distortion of the magnetic field of the electromagnetic tracking system or as the mechanical shaking and hysteresis of a robot actuator.
When the effects can be seen as random variables having a Gaussian distribution, the averaging based on the state's covariance is a real information fusion based on the stochastic properties of the input streams.

So the algorithm proposed, the Federated Kalman Fusion, is a valid fusion scheme and suited to process multiple modalities.
The choice of modalities itself then determines if they are suited for endoscopic tracking or not.
A mechanical link or robotic arm can only be used for tracking as long as there is a rigid connection between tracker and endoscopic tip.
The TRUS probe (see figure~\ref{fig:EndoTofPet_internalView}) provides such a rigid link.
In case that the rigid link propagates to the outside of the patient's body, also optical tracking can be employed and would be a suited and very precise means of tracking.
However completely inside the body, e.g.\ in case of a flexible endoscope, only electromagnetic tracking is well suited.

In the case of electromagnetic tracking, the filter will assume a higher measurement noise than with optical or mechanical input.
It can thus account for stochastically independent effects that impoverish the EMT measurements, and will rely more on its estimated course of motion.
With the IMM, such internal motion models can be combined in order to account for mixed motion types (curves, sine motions), while still only computing linear motions in each individual filter.

\paragraph{Selection of devices suited for sub-millimeter accuracy}
From the preceding publications on the accuracy of current electromagnetic tracking, it can be stated that this tracking form has the capability for reliable sub-millimeter accuracy~\cite{MaierHein2012}.
In order to track an endoscope tip inside the human body, other technologies can be thought of: three-axis-fluoroscopy, 3D ultrasound, optial fiber Bragg sensors and radioactive markers.
All of these do have the capability to provide a very precise tracking, but commercially available products are not known and the possible inflictions to the patient not as much tested as they are with the established electromagnetic tracking.
Either the radiation dose could be too high in fluoroscopy and radioactive markers or the cost introduced by having an additional robotically guided 3D ultrasound device in the OR not be worth the benefit a sub-millimeter tracking provides.
In the case of the \Endo project this would be a very precise image reconstruction of ultrasound and PET simultaneously.
It was mentioned that the desired pixel-resolution could be as low as 0.6\,mm, see~\cite{Zvolsky2013}.
Such an accuracy is very close to the static accuracy of EM tracking devices.

Since the scanning and tracking inside the patient might also be desirable in motion, a need for the description of dynamic tracking accuracy arises.
Taking all effects into account that could be observed during the experiments, a formula can be derived to describe this accuracy:
\begin{equation}
A = \mathrm{\textit{system error}} + \left(\mathrm{capture~delay} + \mathrm{sync~error}\right)\cdot\mathrm{speed} + \mathrm{\textit{tracker error}}~,
\label{eqn:TotalAccuracy}
\end{equation}
where $A$ is the total accuracy, the \textit{system error} is comprised of the inherent device accuracy plus the calibration error (see section~\ref{sec:cal}) and the \textit{tracker error} is the misalignment of the optical target to the segmented sphere positions or the residual error after solving the EM-field equations (see section~\ref{sec:Polaris_start}).
The speed-dependent term can of course be removed if the device is not in motion during a recording.
Otherwise, as the capture can never be taken instantaneously, there will always be a certain blur of the tracked object.
This capture delay is usually not very large, but a synchronization error can be much larger.
If two tracking streams are misaligned in time (see figure~\ref{fig:DelayScheme}) about 50\,ms and the endoscope is moved with 100\mms, one sensor will give readings that are up to 5\,mm wrongly aligned in space.

What can be done with the Matlab program developed during the thesis, is a partial reduction of the \textit{tracker error} term.
Similar to Feuerstein and Nakamoto (see~\cite{Feuerstein2007}, \cite{Nakamoto2008a}), the EM tracker pose is always compared to the optical tracker pose.
When a deviation of this pre-calibrated pose difference $X$ should arise and the OT data are flawless at that moment, the deviation can be corrected by mapping the OT pose to the EMT frame.
For EM trackers such a deviation will most likely happen due to a ferromagnetic disturbance of the generated EM field.
But if a marker of the optical target is partially of fully occluded at that time, such a correction of the EMT can not be employed.
Instead, the OT deviation has to be dealt with somehow.

In the presented Federated Kalman Fusion, the OT system would report a higher error due to the misaligned optical tracker.
This error is then added to the measurement noise matrix $R$, so that the optical measurements are not taken into account too much.
But the Kalman Filter being a recursive estimator will only look at the current measurement and the previous state, not further back.
So in each step, it will weigh its own prediction more than the corrupted measurement, but still go a bit towards the direction of that measurement.
If the corruption through partial tracker occlusion yields a constant offset, the error is not Gaussian-distributed anymore and the Kalman Filter will follow the corrupted measurement after some time.
By tuning the $Q$ and $R$ matrix to optimal values for such occlusion cases, a stability over some seconds can be reached, but after that the estimate will be fully in union with the measurement.
The advantage of a Federated filter scheme here is, that if at least the EMT system is correct at the time, its higher weighting in the fusion step will pull the estimate more towards the EM tracker's measurement and therefore estimate.
Vaccarella~\etal again use the EMT pose multiplied by the calibrated $X$ transformation, in order to reconstruct the optical marker that is being occluded~\cite{Vaccarella2013}.
So using their scheme, one modality is corrected by the other and vice versa, but the combination of the result is not covariance-based so that constant weights have to be set before starting the algorithm.

Having explained the possible hazards for a tracking fusion system, it should get clear now that the Federated Kalman Filter should not output estimates that are worse than its worst modality.
This does not sound overwhelmingly good, but considering that all other modalities are misaligned by erroneous synchronization (5\,mm as above), distorted by occlusion or simply in bad shape (see figure~\ref{fig:BadSpheres}), this can still be considerably better than a simple non-weighted mean of all modalities' estimates.

\begin{figure}[htb]
\centering
\includegraphics[width=0.9\textwidth]{GoodBadSpheres}
\caption[Comparison of optical targets]{Example for different optical target qualities.
The upper target (1) had reflective spheres that were worn out, which is already visible in the colour image.
The corresponding infrared image (A) shows the raw image that the optical tracking system (NDI Polaris) receives and processes.
The spheres' centers are computed in order to determine their location in space through stereoscopic methods. If those centers are not found correctly, the error is passed on to the target's pose calculation.
The lower target (2) has clean spheres, which leads to a much better image (B). Target (1) had a residual fitting error of 0.5\,mm and target (2) an error of 0.1\,mm in comparison.}
\label{fig:BadSpheres}
\end{figure}

\paragraph{Development of new tracking paradigms that allow a higher robustness to occlusion, distortion, failure of single sensors or temporarily dropped sensor readings}
The tracking paradigm of using a Federated Kalman Filter to continuously integrate all available trackers in a medical setting was not found in the recent literature.
Continuous sensor fusion is known in aeronautics and advanced vehicle position estimation, as well as in recent consumer electronics (Nintendo Wii, smartphones).
But for the operating theatre it is only researched how to switch between modalities or how to make one single modality very precise.
In a federation of filters, several weak position informations can be fused to one strong description, always under the assumption that Gaussian noise is the cause of inaccuracy.
If all sensors in the federation have a constant position offset of +5\,cm in z-direction, also a flawless, perfectly calibrated IMM filter cannot determine the true position.

So the described technique is new for the field of medical navigation.
The very special case of a Federated Kalman Filter using several independent IMMs as \textit{Local Filter}s, which again combine several Kalman filter models, was also not seen in the reviewed literature.
It could also be called 'Federated IMM Kalman Filter', which would describe the algorithm more precisely, but that might be unnecessarily long.
As the motions of a hand-held device are yet smooth in time, i.e.\ the moved tool has a certain inertia and has to follow the rules of basic kinematics, the physician's motions are very unconstrained.
It is therefore definitely necessary to combine multiple motion models in order to predict the next pose within somehow reliable margins.

Not only should the developed technique be new, but also robust to certain error sources.
In the case of occlusion, the measurement error $R$ is increased.
If the reported error gets too large too fast, it is possible to drop the sensor stream altogether to not affect the fusion result with a spike in the erroneous direction.
Distortion can be dealt with in the same manner, also the correction over optical tracker mapping through $X$ was implemented.
Temporarily dropped sensor readings can be handled for a few seconds, by just using the Kalman prediction step as an estimation result (compare~\ref{sec:kalman} and~\ref{sec:fusion}).
Naturally, this prediction will diverge from the tool's motion if it is not by chance exactly moved in a perfect static, linear or accelerated motion, but that is rather to expect from a robot than from a medical hand-held tool.
So some frames can be skipped using the prediction, but it is not a long-lasting solution.
If too many readings are dropped, a cancellation of the stream altogether could be beneficial.


\chapter{Outlook}
\label{ch:outlook}
\section{Real-time tracking fusion}
\paragraph{C++ implementation}
The presented schemes of synchronization, calibration, fusion and visualization were implemented in Matlab.
For the future, the most promising parts of the implementation should be ported to a compiled programming language like C++, in order to achieve very fast computing times.
The processing of different input streams, update of the multiple Kalman Filters, calculation of the IMM model probabilities and the output should then be achievable with a high framerate, e.g.\ 25\,Hz, so that a smooth display in a GUI is possible.

\paragraph{Using model probabilities}
In formula~\eqref{eqn:TotalAccuracy}, the high influence of motion speed was made clear.
When going for the most precise position information in order to take an ultrasound image or just to determine a fused position, it can therefore be good to hold the tool completely still for that time.
The remaining Gaussian measurement noise would then be filtered out optimally by the 'constant position' Kalman Filter or by an IMM that has switched to that static motion model.
So when the real-time tracking fusion is implemented, one possible use-case for the future could be a graphic object in the GUI that indicates which IMM model is active at the moment, e.g.\ by a colour indicator.
If the 'constant position' model loses the highest probability during a static scan, a warning can be activated to hold the probe still.

\section{Complete prototypical set-up}

Figure~\ref{fig:TheWholeThing} shows, what a tracking fusion-framework would be able to do if all presented technical possibilities were built into a real-time application.
In order to make the topology as versatile as possible, a network-based connection of all tracking devices and workstations using the CAMPCom framework is targeted~\cite{Schoch2013}.
To solve the critical factor of millisecond-precise synchronization, an implementation of the Precision Time Protocol (PTP) would be needed.
Then, all the techniques from calibration to fusion, the output of an uncertainty value to the IMM model probability, could be computed in a distributed fashion and streamed to several visualization clients.
\begin{figure}
\centering
\includegraphics[width=\textwidth]{TheWholeThing}
\caption[Schematic of the full tracking-fusion framework]{Schematic of the full tracking-fusion framework.
Real-time fusion would be possible with a scalable number of different input devices, yielding a continuous estimation with a steady common output rate.
Several tracking devices send their readings over the network, the data are processed and different motion models are mixed for robust pose estimation.
Single estimations are then fused and the common covariance $P$, the fused state $x$, the motion model probabilities $\mu$ and possible warnings (red, yellow and green dots) are provided to the network at a scalable frequency.
Several users can access the data, all clocks are synchronized over the PTP protocol.
The tracking can be used for guidance, mobile monitoring or serve as input for a reconstruction algorithm.}
\label{fig:TheWholeThing}
\end{figure}

\section{Application in ultrasound compounding}
The fully equipped tracking, fusion and error-handling solution could be tested for ultrasound compounding.
Here, several 2D ultrasound tomographies are recorded while the ultrasound transducer is being tracked.
This way, the tomographies can be correctly aligned into a 3D volume.
Here, interpolation onto a fixed voxel grid can be performed and new slice directions can be computed.
This procedure is already in use by companies as Brainlab or Curefab, but they rely on one tracking modality at a time.
Naturally, the operator has to be carefully avoiding metallic objects if the US probe is tracked via EMT.
When the commercial product uses optical tracking, the hand-held US probe has to be manoeuvred so that a steady line-of-sight is established between probe and tracking cameras.

With the presented fusion framework the workflow could be made somewhat more comfortable, as the user does not have to care if every tracker is available.
If the algorithm sees one tracker it uses that tracker.
When another tracker comes into sight, it is weighted with its current covariance and might improve the estimate about some millimeters.
In the case of a full tracker occlusion or distortion, the recorded slices can be automatically discarded and warning can be produced to scan the area again.

This possible application of a multi-sensor fusion would make tracking in the medical field more user-friendly and the processes more fluent and care-free.
As the covariances are steadily updated, a measure of reliability can be always provided together with the recorded data.
This would give the user the full control and a deeper understanding about what he actually recorded and help to further evaluate and improve the fusion for different modalities.

\vspace{0.5cm}

The necessary pre-conditions for precise multi-modal tracking were investigated in this thesis.
A new, probabilistic fusion-algorithm was developed that is versatile using multiple motion models and that can handle various input streams at different sample frequencies and with different accuracy specifications.

%\subsection*{IMM implementation}
%\label{subsec:OutlookIMM}

%\subsection{Network synchronization}
