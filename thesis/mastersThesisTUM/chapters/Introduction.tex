\newcommand{\numpapers}{50}
\newcommand{\numlines}{40000}
\chapter{Introduction}
\label{chapter:Introduction}

The research for this Master's thesis was conducted at the Chair for Computer Aided Medical Procedures (CAMP) of Professor Nassir Navab at Technische Universität München.
Advisor was M.Sc.~Bernhard Fürst (CAMP), supervising professors were Prof.~Nassir Navab (CAMP) and Prof.~Olaf Dössel (IBT Karlsruhe).
The duration of work was about six months, in which two student assistants and two Ph.D.\ students contributed greatly to the effort of finalizing this work (see Acknowledgements \pageref{chapter:Acknowledgements}).
During the process a review of \numpapers~papers was undertaken to find out the current state of the art as well as faulty or uninvestigated approaches to tracking and navigation for medical applications.
For signal recording, post-processing and visualisation \numlines~lines of matlab code were produced \todo{The most useful and well written parts of the code were made publicly available via the Mathworks File Exchange and an Open Source server of the CAMP chair}.
The aforementioned researchers also helped submitting conference papers which cover a part of the results and methods presented in this thesis.

%\paragraph{General thoughts} A master's thesis is commonly regarded as the first real scientific work of a university student.
%It provides access to a higher academic level for some or guarantees a better position in salary negotiations in the economy for others.
%In each case, it generally marks the border between the unpaid and independent student world to the (more or less well) paid and therefore very depending world of work.
%Another interpretation could hence be that a master's thesis is the scientific contribution that is most free of any financial pressure or dependency on specific interest groups.
%Being written under such special conditions, it can provide benefits very different from other scientific contributions.
%A company's white-paper gives an insight into the developed techniques that is certainly limited by pending or granted patents as well as it hides critical parts due to fear of competitors in the field.
%The first and foremost criterion for journal or conference papers is to be accepted by the respective reviewers.
%Also they might be submitted to contribute to a certain minimum number of papers a Ph.D.\ student has to submit in order to achieve his or her title.
%Another important kind of contribution are introductory or specialised books in the context of university teaching.
%Also lecture notes and publicly available slides are of major importance.
%Yet those kinds of scientific contributions have a very different focus and mostly do not deal with  practical problems, specific ways of implementation or the greater view of inclusion into frameworks, research collaborations, funding or significance for final applications.
%That is not to say those topics should be the content of first-semester courses, there need to be certain fundamentals of pure theoretical nature in order to move on to more detailed courses and specialised fields of research.
%But in the process of studies it could be desirable and useful to introduce students to those topics, in order to provide motivation and to create a better public understanding of science.
%This would be most useful to identify faulty contributions to the community, which are clearly not science.
%Examples are studies that were clearly undertaken to promote products, companies or certain kinds of merchandised lifestyles\todo{cite faulty papers}.
%Although those black sheep are known to be more prevalent in the fields of pharmaceuticals, food sciences and marketing,\todo{citation of comparative analysis} one has to be aware of their existence and also stay cautious in the very profitable field of biomedical engineering and informatics.
%A last and also least kind of science (together with the studies only conducted to advertise a certain product) that has come to public attention in the last years are partly or completely copied sections in doctoral theses\todo{cite guttenberg, Schavan, all the rest}.
%To prevent those cases from happening, an early and thorough introduction should be given to students regarding scientific writing, correct citation and what harm cheating does to the community.
%Making use of the world wide web, all universities should implement automatic systems to check for potential frauds in scientific contributions (or should have done so 15 to 20 years ago).
%
%\paragraph{Purpose of this thesis}
%From the above statements is derived, that a thesis is only as good as its public availability, its open and impartial view on the topic and the worth it provides to the community in respect to re-using or validating the presented methods and results.
%This thesis is not written to be accepted in any kind of journal nor should it advertise any kind of product or company.
%It is supposed to give the reader a thorough and useful introduction into the topic and to present all methods and results in detail, including their uncertainties, faults and shortcomings.
%If any student wants to start a project or thesis in the field of tracking and surgical navigation, this thesis is thought to serve as a useful start.
%Also it is thought to introduce new or at least updated thoughts and methods to the field, to facilitate making improvements to existing products and to enhance the quality and robustness of tracking for medical applications.
%In order to comply with those goals, observations and feedback from critical readers is a crucial tool and will be used to keep track of a list of errata and an addendum made available through the web.

\paragraph{Structure and \textit{special sections}}
This paragraph resembles a "README" where the willing reader can find \textit{relevant sections} faster and plan an efficient way of reading through the work at hand.

In the first part of the thesis, the introduction chapter from page \pageref{sec:intro_start} to page \pageref{sec:intro_end} provides a quick overview of facts and numbers on the thesis as well as general and specific thoughts on scientific contributions.
In the following introduction section a motivation for precise tracking in the operating room is given, followed by a timeline of innovations and classification of \textit{previous papers to the topic} (\pageref{subsec:stoa_start} to \pageref{subsec:stoa_end}).

Theory on algorithms and hardware as well as the derivations of all relevant mathematical formulas is given in chapter \ref{chapter:Theory}.
After a classification of existing tracking hardware, the devices used during this work are described in detail (\pageref{sec:Polaris_start} to \pageref{sec:Aurora_end}).
The next theoretical section is about the synchronization of datastreams.
From the comparison of different techniques that are used for tracking, a \textit{scheme of time delays and uncertainties} is derived on \pageref{subsec:delayscheme}ff.
Synchronization is followed by section\ref{sec:handeye} about Hand-Eye calibration algorithms.
An overview of their fields of use and of the respective equations is given.
A comparison of shortcomings and advantages of the different methods concludes the section.
Motivation for Kalman filtering and the basic mathematical formulas for several types of \textit{Kalman filters are introduced} in section \ref{sec:kalman}.
Since the Kalman filter family is used in various fields, examples of their application are given to show what can and what cannot be done using the filter.
The theory of interacting multiple models (\textit{IMM}s) in section\ref{sec:imm} concludes the theoretical chapter.

To perform a sensor fusion for device tracking, the contents of the theory-chapter have to be applied in the order they are listed here.
If the timing properties of the hardware are unknown, one cannot perform a synchronization.
If the data streams are not synchronized, their position data cannot be used for calibration and so on\dots

The second part of the thesis is comprised by the chapters methods (\ref{chapter:Methods}) and experiments (\ref{chapter:Experiments}).
Final choices of synchronization, calibration and fusion algorithms are presented in the first method sections.
The respective implementations are explained in detail to facilitate the production of a similar tracking fusion by related groups.
After explaining the methods for fusing regular tracking devices, \textit{a new tracking method} using radioactive seeds and a gamma camera is explained in the subsequent section (pages \pageref{sec:Gamma_start} to \pageref{sec:Gamma_end}).
To manage all incoming data streams and visualize the output, the applied communication framework and visualization engine are presented in the last method section.

Chapter\ref{chapter:Experiments} introduces the laboratory set-up and environment in which all experiments were conducted.
Data flow, post processing and recordings scheme are explained and \textit{final result data} are explained in the next sections \ref{sec:recording}, \ref{sec:results}.

The third part provides the reader with an interpretation of result data and an outlook or suggestions to future work on hybrid tracking.
In the conclusion chapter, the performance of the presented fusion algorithm is estimated based on obtained result data.
A \textit{performance comparison} to fusion schemes of other publications represents the second section of the conclusion.

The outlook in chapter \ref{chapter:Outlook} presents different types of possible applications of the developed fusion scheme.
Estimates and preconditions for a real-time implementation are given, and the future \textit{incorporation of additional tracking modalities} into the tracking fusion is discussed.

%\begin{itemize}
%\item general view on masters theses \#done
%\item special purpose of this one \#done
%\item how to read the thesis, in which chapter comes what and why \# active
%\begin{itemize}
%\item Part I (Intro+Theory) done,
%\item Part II Methods and Experiments TODO
%\item Part III Conclusion and Outlook
%\end{itemize}
%\end{itemize}


 
\section{Motivation}

\paragraph{Advantages of tracking} Before introducing benefits and necessities of sensor fusion in the field of tracking in the OR, a motivation for medical tracking in general is given.
Studies that evaluated the effects of tracking techniques on surgeries and diagnostics, state a considerable improvement of intervention time, accuracy and reliability of the outcome.
BlaBla\etal showed that... \todo{Orthopedics papers are prevalent, where are brain, ear and abdominal summaries? sources needed}

Tracking and navigation have made types of operations possible that were not thought of before.
Especially brain, inner ear as well as abdominal surgeries have become a playground for researchers from a technical as well as a medical background.
\todo{Point out minimal openings in skull for tumor removal (Dr Spetzner from Karlsruhe), precision keyhole operations in the inner ear (Da Vinci and other systems) and the SurgicEye technique that was not possible before, as well as 3D US compounding for Brain (Ahmad as source) or for other parts (Ask what curefab dos, Christoph should know some applications, yes he said he does, write him a mail)}


\paragraph{\Endo}
Work for this thesis was conducted in conjunction with the \Endo project.
This EU-7 related joint research programme was founded in 2010.
X departments form France, Germany and UK are working on the development of a combined endoscopic PET and ultrasound imaging device.
This new combined modality can be used for interventional imaging in prostate or pancreatic cancer patients.
Endoscopic ultrasound can be used to create volumetric images of the anatomical structure close to the probe.
Augmenting anatomical information with a 3D reconstruction physiological processes can be achieved with a PET and ultrasound fusion.
Fusing 3D images of thwo modalities always requires knowledge of their respective position in space.
Since both systems would be mounted on the same probe, the spacial relation between the images is known.
Nevertheless PET reconstruction requires two crystal detectors, in this case one inside and one outside of the patient.
Those detectors also need to know their respective position in space.
If the uncertainty of this relative positioning is high, the reconstructed image can only yield a poor resolution since uncertainty is passed on to the reconstruction process.
The mechanisms behind US and PET volume reconstruction are to complex to explained in detail in this introduction part.
A short explanation is provided in Appendix A, which highlights the propagation of positioning errors to image errors. \todo{small PET and endoscopic US explanation in appendix}

To sum up the former part: in order to reconstruct a tomographic image from the recordings taken inside the body, a precise tracking of the tool is necessary (see figure\ref{fig:EndoTofPet_internalView}).
As Electromagnetic tracking might get imprecise due to metallic distortions during the recording or drop some measurements because the sensed field distortion is to high to reconstruct to sensor's pose (see\ref{subsec:EMTexplanation}), preconditions here are a lot more difficult than outside of the patients body.
If the endoscopic tool is rigid, as holds for the transrectal ultrasound probe, an optical tracking of the external part can be employed.
Optical tracking is not affected by metallic distortions, but can be occluded or permanently damaged during the operation due to the optical targets size.
To track position and orientation of an optical target, it needs to be constructed from at least 3 reflective spheres or active infrared LEDs.
Those basic parts of the target need to have distinguishable distances from one another which also need to satisfy a minimal distance constraint for reliable differentiation.
This space-constraint yields a minimal target size of something like 4x3x1\,cm$^3$, which is not huge, but in relation to a 6\,DoF electromagnetic sensor with 2x2x10\,mm$^3$ it seems pretty bulky.

As for the special conditions under which a precise tracking is motivated in the \Endo project, the goals of the work are defined as:
\begin{itemize}
\item creation of a sensor fusion algorithm between multiple modalities suited for endoscope tracking
\item selection of devices suited for sub-millimeter accuracy
\item development of new tracking paradigms that allow a higher robustness to occlusion, distortion, failure of single sensors or temporarily dropped sensor readings.
\end{itemize}


\textit{bullet points}
\begin{itemize}
\item general motivation for tracking, papers which evaluated the effects of OPs with and without tracking, operations that were not possible before
\item special motivation of \Endo, working mechanism of a US and a PET (short), special conditions for the tracking task, goals
\end{itemize}

 

\section{Previous work}

In the past years, many advancements towards continuous precise tracking inside the OR have bee undertaken.
As presented before, not only one modality is available to achieve this valuable goal.
A hybrid per se is a combination of different beings, in technical environments used for a combination of existing techniques that produces a bigger value than the single technologies on their own.
The aforementioned goals have been tackled by various groups, whereas a fusion was often not attempted in favour of a simple switching between modalities.
This can be called a hybrid approach, but of course the maximal benefit is only reached when the interaction between modalities is steady, adaptive and designed for those use-cases where most additional value is created.
A hybrid car with gasoline and electric propulsion does not only start the electrical engine when it runs out of gas and switches back to gasoline after refuelling.
It switches to electrical power when it is most beneficial, in cases of acceleration from low velocities or when rolling down a slope.
Additionally the gasoline engine can repower the battery and \todo{traegheit} energy is transferred back during braking through magnets and coils on the \todo{Antriebsstrang}.
In this very advanced example one can see that hybrid devices can interact very goal-specific to provide more efficiency and comfort for the user.
When the hybrid aspect is applied to information instead of mechanical devices, it intersects with the \todo{Begriff} of fusion.
This is elaborated later on, since information or data fusion itself is a huge field with their own journals and conferences not necessarily associated with medical devices or any specific use-case.

Among the first to use more than one tracking modality in a clinical set-up were Birkfellner\etal in 1998.
They performed all steps necessary to prepare a consistent hybrid use of an eletromagnetc and an optical tracking system for the operating room.
Although the papers do not much go into detail about synchronization, an explanation about the devices spatial calibration and 

\textit{bullet points}
\begin{itemize}
\item what have they done> use hybrid tracking like Feuerstein, correct Em distortion statically and dynamically, switch to EM when OT is not there (Idea of Birkfellner in 98, conducted by Vaccarella)
\item where are the weaknesses> loss of information, dependency on one perfectly calibrated modality
\item what is to be achieved in this thesis and where is the value in that for others (contribution)> fusion framework that uses a variable number of inputs and outputs one tracking stream at a scalable frequency
\end{itemize}

%One common use-case is the guidance of the surgeon to regions of interest or along pre-operationally defined paths.
%Fields where computer assisted guidance could be established range from bronchoscopy\cite{bronchoPapers}

