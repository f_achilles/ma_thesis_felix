%% content.tex
%%

%% ==============
\chapter{Methods}
\label{ch:methods}
%% ==============

Methods to establish hardware, communication and post-processing are presented in this chapter.
Also the implementation of calibration, synchronisation and fusion algorithms is explained in detail.

\section{Experimental set-up}
\label{sec:setup}

All experiments for sensor fusion were conducted in lab environments.
In order not to disturb the electromegnetic fields generated  by the EM tracking device, metals were kept at a minimum distance of two times the measurement volume.
Most methods were tested on two tracking modalities, EMT and OT.

\subsection{Tracking hardware for EMT and OT}
\paragraph{Features}
The electromagnetic tracking was conducted with an Northern Digital Aurora Tabletop Field Generator (Tabletop FG).
Northern Digital (NDI) has released the device in November 2011\footnote{\url{http://www.ndigital.com/medical/pr-AuroraTTFGrelease2011.php}}, including an interesting feature that makes this field generator more suitable for use on metal surgical tables than previous designs.
This feature is an EM shielding of the FG against all metallic objects underneath the generator plate.
This plate measures $762\times507\times34\,$mm l$\times$w$\times$h and emits a field suited for millimeter-precise tracking with a volume of $600\times400\times500\,$mm l$\times$w$\times$h above the generator.
The tracking volume is suited to cover the full abdominal region of a patient lying on the generator plate.
According to a very diligently performed accuracy evaluation study of NDI's three most recent field generators by Maier-Hein\etal, the Tabletop FG achieves a mean static accuracy of $0.9\pm0.05\,$mm in position and $0.8^\circ$ in orientation, standard deviation of the error was not provided for orientation~\cite{MaierHein2012}.
Update rate in six degrees of freedom (position and orientation) is 40\,Hz, also when a maximum of 4 sensors is connected to the according data processing unit.
%\todo{Cite Aurora manual, aurora website, Maier Hein}

Optical tracking was performed using the NDI Polaris Vicra stereoscopic infrared tracker with passive reflective spheres.
The Vicra is a downgraded more compact version of its bigger brother NDI Polaris Spectra, but achieves almost as good accuracy values with $0.19\pm0.14\,$mm position and $0.38^\circ$ orientation error, standard deviation of the orientation error was not provided~\cite{Wiles2004}.
It can detect NDI manufactured or selfmade tracker configurations made from three to 20 reflective spheres with a sample rate of rather slow 20\,Hz.
Here one can already see a potential benefit of a continuous fusion, as EMT data are available double as often and other devices with even higher update rates could enhance the availability of tracking information even more.
The provided software ''NDI architect'' allows to create geometric profiles of the selfmade trackers.
It additionally provides the user with a quality check that tests if the tracker geometry fulfils certain requirements that ensure the performance stated by NDI and tested by Wiles\etal~\cite{Wiles2004}.

\paragraph{Weaknesses}
While the Tabletop FG is still vulnerable to metallic distortions introduced by objects in the tracking volume above the generator plate, the Vicra provides faulty tracking data if one of the infrared markers is occluded during a recording.
Both systems provide the user with error flags and trust values in case that such errors occur.
Those flags only switch to active when a distortion or occlusion is clearly noticed, i.e.\ when it supersedes a certain built-in threshold.
A dangerous case for high-accuracy tracking is therefore an introduction of small metallic objects into the EM volume or a partial occlusion of an optical marker, that would result in an error in marker center calculation which is then propagated to the tracker-pose calculation.
Those two types of errors cannot be caught by the devices' routines and therefore have to be filtered or detected in a different fashion.
Here it becomes clear that an individual adapted filtering of each tracking stream might prove beneficial, since every tracking modality has its own shortcomings.
To apply one single filter to all devices ought to perform worse as it overlooks distinct properties that  the respective technologies have.

\subsection{Tracking hardware for other modalities}

Other modalities have been tested for tracking and first measurements have been taken.
In the thesis' outlook (chapter~\ref{ch:outlook}) the goal of fusing those and other modalities into the developed system as well is illustrated.
Their possible added values for tracking are shortly discussed here and system specifications are given for completeness.

\paragraph{Static field EM tracking}
The Ascension corporation was recently acquired by NDI, so their name now only denotes a specific branch of NDI devices, not a company name anymore.
An Ascension TrakStar device was available to our department for some time, as a cooperating research group was able to lend one.
This tracking system works similar to the NDI Aurora, so equations from section~\ref{sec:Polaris_start} also apply to their wired sensor coils and generator coils.
But instead of emitting RF signals in the kilohertz range as the Aurora, the TrakStar system emits pulses of static fields which alternate over three emitter coils.
A benefit of the system is its very quick sampling, which provides up to 420\,Hz data rate.
An important shortcoming when targeting sub-millimeter accuracy however is its positioning error of $1.4\,$mm RMS and a greater affinity of static fields to metallic interference.
Furthermore the current emitter system is not shielded to any side, so that an additional calibration procedure (\textit{static distortion correction}~\cite{Nakamoto2008}) has to be applied each time the field generator is placed close to a metallic table or e.g.\ a CT device in the operating room.

\paragraph{Mechanical tracking}
A Faro measurement arm was used to test hand-eye calibration and the influence of its high accuracy and update rate.
The arm possesses a static accuracy below $0.02\,$mm and 250\,Hz data rate for 6\,DoF pose information of the probe tip.

\paragraph{Robotic tracking}
The model UR-6-85-5-A manufactured by Universal Robots was used for a recording due to its active motion properties, high accuracy of $0.1\,$mm and high 6\,DoF data update rate of as well 100\,Hz.
The stated accuracy does not hold in case of motion, since the motors and joints create a certain hysteresis and noticeable shaking of the end-effector.
To capture the true position between two static stations, the robot hand was also tracked by a Polaris Spectra with the accuracy stated above and an update rate of 60\,Hz.

%% ===========================
\section{Data acquisition and generation}
\label{sec:acq}
%% ===========================

\subsection{Synthetic data}

As the two devices of the above selection that have the most practical value in an operating theatre, the NDI Polaris and the NDI Aurora Tabletop FG were used extensively for data recording and algorithm testing.
However in order to achieve a sub-millimeter tracking accuracy in motion, their specifiations would have to be 1 to 2 orders of magnitude better to be applied as actual ground-truth data.
In the scope and financial margins of this thesis no machine was affordable that could have fulfilled all the requirements, which would be microsecond-precise timestamps, delay estimation between device and workstation, $\sim\,1000\,$Hz sample rate and a position error below $0.05\,$mm.
Maier-Hein\etal envision that a joint research group will tackle the technically extremely difficult task to evaluate tracking in motion in 2014~\cite{MaierHein2012}.
The above numbers in mind, such a precise tracking hardware would be very valuable, but until then we decided to use synthetic data for an evaluation against ground truth.

The data were constructed simulating continuous motion patterns in 3D.
Two devices rigidly connected by a Hand-Eye relation were assumed, each having their own local coordinate frame.
Beneficial about the synthetic or simulated tracking data approach is that problems as synchronisation and spatial Hand-Eye calibration are already solved, the respective time and pose values are always known.
Noisy data could be produced and the respective Kalman filter (see~\ref{sec:kalman}) could be fed with exact noise covariances to make use of its optimal performance.
For this thesis most of the code was Matlabs m-code, so that synthetic data was also computed using Matlab.
The data format was chosen to be a struct, as it provides the most flexibility when a later design decision is made to incorporate more information into a sampled datapoint.
For now, a struct \code{data} contains the fields \code{data.position}, \code{data.orientation}, \code{data.TimeStamp} and \code{data.valid}.
The \code{.valid}-field can be used to simulate an occlusion or a detected distortion of an EM tracker that supersedes the allowed error tolerance.

\subsection{Real world data}

In each recording of tracking streams, the used devices were connected to the same workstation to facilitate timestamping and therefore synchronisation issues.
C++ scripts were used to poll pose data with a high frequency >1000\,Hz, to not miss any pose update provided by the device.
All mentioned devices provided drivers and an Application Programming Interface (API) that could be implemented more or less directly.
For the NDI devices a wrapper was written by Alexander Schoch, in order to incorporate them into the CAMPCom communication framework~\cite{Schoch2013}.
CAMPCom provides an easy-to-use client-server interface in which tracking or other data can be streamed in closed local area networks.
This facilitates using one machine for data polling that is set up next to the tracking bench.
Data can then be forwarded to a common server machine and polled by a visualisation and fusion workstation with only millisecond delay time.
Data streaming was tested and a video was created that shows if only OT, EMT, both or none of them are currently available\footnote{\url{http://www.youtube.com/watch?v=3EzxtWoqUug}}.
The data polling was done by C++ code, the streaming over CAMPCom and the visualisation used the Matlab Engine in C++ that received and displayed current poses in a 3D plot.

In order to minimize the danger of packet loss or polling the devices too slowly, recordings for offline processing were performed only with the API code and the respective poses were written to file.
Those files were then fed to Matlab functions that averaged over static positions for the calibration modules or that processed continuous trajectories by finding out and marking invalid data as in the \code{data} struct explained above.
The created structs from real tracking experiments had an additional field, namely \code{data.DeviceTimeStamp}, that was used to test the quality of device-time synchronisation~\ref{sec:sync}.


%% ===========================
\section{Calibration}
\label{sec:cal}
%% ===========================

As presented in section~\ref{sec:handeye}, there were various Hand-Eye calibration techniques developed since Tsai\etal published their famous Least-Square solution~\cite{Tsai1989}.
The final choice for this thesis was a combination of two techniques that showed good performance in their original design and could be equipped with an efficient motion selection scheme to improve the condition of the $AX=XB$ equation.

\subsection{Tracker-to-tracker calibration, the \textit{X} matrix}

The developed algorithm can process an input of various pre-recorded or offline data streams from different devices.
The $X$ transformation of all secondary device trackers to one master tracker will be calculated.
In our case the master was the optical tracking stream but this could be chosen differently.
As it is beneficial to use all pose-pairs or motions computed from the input stations, the number of motions $M$ is related to the number of stations $N$ as $M=\frac{N(N-1)}{2}$.
When a recording of distinct static stations was performed, $N$ is probably around 20 to 30, so that $M$ and the number of computation cycles will stay in a tolerable margin.

\paragraph{Motion selection}
If however a continuous recording is to be used for calibration, a one-minute recording at 40\,Hz data rate will produce $N=2400$ stations, that result in $M=\frac{2400(2400-1)}{2}=2,878,800$ possible pose-pairs!
This huge amount of possible combinations is too much for a scripting language like the Matlab code, but would force even compiled programs to compute several minutes or even hours if an optimizer-based calibration is employed.

Thus a pre-selection based on the inter-station rotation and the difference of the rotational axis of the respective candidate to the axes of already accepted pose-pairs is performed.
Those two measures are the most critical requirements for a stable and accurate calibration and were therefore chosen for the motion selection.
If a candidate motion does not reach a threshold in rotation and axis difference (both expressed in degrees), it is skipped and not considered in the following calibration.

\paragraph{Two step calibration}
The pre-selected motions are then processed with Tsai's algorithm, which solves first for the rotational and then for the translational part of $X$.
It does both steps in the form of solving an over-determined linear equation system, which is usually written as
\begin{equation}
Ax=b,
\label{eqn:linear}
\end{equation}
where $A$ is an $n\times m$ matrix with known values, $x$ an $m\times1$ vector which is unknown and $b$ the known result vector with the dimenstion $n\times 1$.
If $A$ was regular, the equation could be multiplied by $A^{-1}$ from the left and one would already see the exact result $x=A^{-1}b$.
As in the case of Tsai's solution $A$ has more rows than columns, a inversion of the matrix is not possible.
Instead, the pseudo-inverse is used which, when applied as a multiplication form the left side, can be written as $\textit{pinv}(A)=(AA^T)^{-1}A^T$.
Solving the linear system with the pseudo-inverse commonly yields only an approximation for $x$ which will be denoted as $\hat{x}$.
It holds
\begin{equation*}
\hat{x}=\textit{pinv}(A)b
\end{equation*}
and luckily the approximated $\hat{x}$ fulfils a least-square condition $\hat{x} = \underset{\tilde{x}}{\arg\min} \left| x-\tilde{x} \right|^2$.
This means the approximated solution is as close the true solution as possible under the $L_2$ norm.

Still there will be some lines in equation~\eqref{eqn:linear} that are solved well by inserting $x=\hat{x}$, and others that are not solved well.
The residual error vector after the least-squares solution can be computed as:
\begin{equation*}
b_{res} = b-\hat{b} = b - A\hat{x} = (I - A(AA^T)^{-1}A^T)b ~~~.
\end{equation*}
From the element-wise absolute values of $b_{res}$ it can be seen for which motions the computed $X$ solved the calibration well and for which not.
It is assumed that those which had a high absolute value in $b_{res}$, introduced an numerical instability in the computation of  $\textit{pinv}(A)$ since they were too similar to another motion in the accepted candidates.

The iterative post-selection by removing those motions from $A$ and $b$ that introduced the highest residual error in each iteration is performed until the maximum residual error falls under a certain threshold (we selected 1\,mm for sub-millimeter accuracy) or until only a minimum number of motions are left.

After achieving a low residual error with this iterative version of Tsai's method, the result $X_{\text{Tsai}}$ is used as a starting point for an optimization scheme.
The optimization scheme again starts with all pre-selected motions and performs the same cancelling of bad motions after it converged to respective local minima.
Many optimization schemes for Hand-Eye calibration are available.
As it was the most recent and very fast and interesting solution, we implemented Zhao's method~\cite{Zhao2011}.
To test Strobl's automated weighting optimization or Schmidt's vector quantization method for pre-selection of motions (both from 2006) would be interesting for a future evaluation, but was too much to implement for this thesis~\cite{Schmidt2006, Strobl2006}.

\subsection{Base-to-base calibration, the \textit{Y} matrix}

As was explained in the theory chapter, it is possible to first solve the calibration problem for $X$ or first solve for $Y$, or even to solve for both simultaneously as in~\cite{Strobl2006}.
We decided to first solve for $X$ in a very robust and precise manner, and then determine $Y$ based on all $N$ recorded calibration stations.
For a single station $i$ holds:
\begin{equation}
Y G_i = X C_i,
\label{eqn:XandY}
\end{equation}
where $G_i$ and $C_i$ are the local frames of grabber and camera, or in our case of an optical and an electromagnetic tracker.

Equation~\eqref{eqn:XandY} has to hold for all stations, as the two tracker devices were always rigidly connected and it was made sure that the bases, i.e.\ camera and generator plate, were not moved during the recording.
Additive Gaussian noise is assumed for all measurements and a constant error is assumed in the calculated $X$.
To minimize the effect of those error-sources, a technique to approximate the median, mean or least-square fit of the possible $Y$'s has to be used.
The iterative closest point method (ICP) was employed for the minimization of translation errors.
Rotation errors are also minimized implicitly during the point-fit, as the relative rotation of the two point clouds also needs to be minimized when all points are to match each other.
To provide a good starting point for the ICP, the mean $\bar{Y}$ was built from all single $Y_i$ from equation~\eqref{eqn:XandY}.
This mean homogeneous transformation cannot be built by simply adding up all solutions and dividing them by $N$.
A simple mean calculation like this would destroy the non-linear unitarity constraint (equation~\eqref{eqn:Unitarity}) that has to be met by the rotation part $R_i$ of $Y_i$.
\begin{equation}
R_iR_i^T=1
\label{eqn:Unitarity}
\end{equation}
This can bee seen by averaging two rotation matrices in a naive fashion and applying the constraint:
\begin{equation}
\label{eqn:UnitarityHurt}
\begin{aligned}
R_a 		&= \frac{R_i + R_j}{2} , \\ \\
R_aR_a^T 	&= \frac{(R_i + R_j) \cdot (R_i^T + R_j^T)}{4} =\\
			&= \frac{R_iR_i^T + R_jR_i^T + R_iR_j^T + R_jR_j^T}{4} = \\
			&= \frac{1 + R_jR_i^T + R_iR_j^T + 1}{4} ,
\end{aligned}
\end{equation}
where the last row is only 1, if $R_j$ and $R_i$ describe the same rotation i.e.\ are equal.

Having shown the inapplicability of direct averaging, another technique was used to produce valid averaging of rotation matrices.
Matrix exponential \textit{expm()} and matrix logarithm \textit{logm()} are applied with optional averaging weights $w_i$ as follows:
\begin{equation}
\label{eqn:LogAverage}
\begin{aligned}
\mathrm{sum}	&= \Sigma_i \left\lbrace w_i \cdot \mathrm{logm}\left(  R_i \right) \right\rbrace\\
R_a^*				 	&= \mathrm{expm}\left( \frac{\mathrm{sum}}{\Sigma_i w_i} \right) .
\end{aligned}
\end{equation}
This matrix average $R_a^*$ is a valid rotation matrix fulfilling the unitarity constraint.
Proving that is a bit lengthy but not hard to do.
Just imagine that when multiplying $R_a^* \left[R_a^*\right]^T$, similar cross-terms as in~\eqref{eqn:UnitarityHurt} will appear.
But the same not being rotation matrices but the matrix logarithm of them, adding $r_j^*r_i^{T*} + r_i^*r_j^{T*}$ actually resembles a multiplication in non-logarithmic space.
When one realises that $r_j^*r_i^{T*} = [r_i^*r_j^{T*}]^T$, and knows that the transpose of a rotation matrix is a rotation in the opposite direction, a multiplication of the respective matrices means the same rotation back and forth, which resembles the identity transformation 1 with $\mathrm{logm}(1)=0$.
So all cross-terms disappear and for the rest $R_iR_i^T=1$ is already ensured as valid rotation matrices were input to the calculation, hence $R_a^*$ is also unitary.

The initial solution for $Y$ was calculated by the logarithmic averaging shown in equation~\eqref{eqn:LogAverage} and then iteratively improved through the ICP method.
This results in a quick and precise solution for base-to-base transformation $Y$, which can be performed after the set-up was changed or device parts were unintentionally moved by colleagues.


%% ===========================
\section{Synchronization}
\label{sec:sync}
%% ===========================

The final choice about how to synchronise the data streams was in favour of central time-stamping with workstation timestamps.
The boost library for C++ provides a very easy way of timestamping the data arrival on a nanometer-scale.
Scheduling times of the polling thread and the polling period itself are of course much larger than nanoseconds, so the precise \textit{boost::chrono} library is just used to not limit the synchronisation further.
For the mostly used devices, the Aurora and Polaris system, we were provided with the internal delay times by NDI, which made central timestamping the most precise method (section~\ref{par:MasterSync}).
However those computation times can not be verified and are not openly available, probably in favour of a better sales position and competition secrecy.
It is not necessary for the manufacturers to provide these information to all customers, since those who incorporate a tracking device into their own product and want to license it, will have to find out this specific information and will be provided with it on demand.
However from a scientific point of view and from a point of applying tracking in surgeries (!), to openly provide exact knowledge to the user of what the system can do and what it cannot do should be of mutual interest and also a matter of common sense.
Uncertainty is inherent to any measurement system, and the knowledge about it is just as important as the measurement itself.
% maybe find works that recognise systems only due to their noise, not to their original funciton or output

Yet, knowing the (secret) internal calculation latency and assuming transmission delays to the workstation to be equal for the modalities, a centralized scheme of data synchronisation was chosen.
Before knowing the internal times and to be independent from such device-specific information, synchronising the devices over a sharp-edged motion trajectory was tested.
But the variance of resulting time-shift calculation was too high (spanning several sample-times), at least for sub-millimeter accuracy in motion.
Better and repeatable trajectories for synchronisation could be investigated with the help of a robotic arm, but this will be subject of future work.



%% ===========================
\section{Fusion}
\label{sec:fusion}
%% ===========================

The aforementioned types of fusing two or more data streams were considered, while a new and more generic and at the same time practical approach was desired.
Vaccarella\etal used one single Kalman Filter fur the fusion, whose measurement input was just switched to whatever modality was available, while optical tracking was preferred.
Only applying one filter meant that the $K$ and $P$ matrices converged to a \textit{steady-state} while only one modality $i$ and its respective $R_i$ were fed to the filter.
If the LOS is lost for optical tracking, the converged matrices are used to weigh EMT measurements against the filter predictions.
As those weightings will first have to converge to an optimal value for EMT tracking, the first samples will not be optimally estimated, this equally happens when changing back to OT measurements.
Another common scheme in GPS and Inertial Measurement Unit (IMU) fusion for vehicle tracking is to take every input available or if both are available at the same time, have a double-sized measurement vector $\hat{z}$.
In the last case, one has to use an augmented measurement-covariance matrix $\hat{R}=\begin{bmatrix} R_i & 0\\ 0 & R_j \end{bmatrix}$ without knowledge of the cross-variance terms which are most probable not zero.

\subsection{Federated scheme: Local Filters}
Those two schemes did not seem optimal for a multimodal sensor-fusion, so that a Federated Kalman Filter approach was favoured.
In the 'federation' of many Kalman Filters, each one for its respective modality, no back and forth switching of the $K$ and $P$ matrices occurs, and each filter can be optimally tuned to its modality.
Figure~\ref{fig:FederatedOne} shows how several filters are applied and provide either updates or predictions (defined in equations~\eqref{eqn:KalmanPred},~\eqref{eqn:KalmanUpdate}), depending on the availability of measurements.
All those local filters are programmed to output estimates $x_i$ at synchronous times, equally important is the output of the respective state-covariances $P_i$.
Those are forwarded to a \textit{master filter} as depicted in figure~\ref{fig:FederatedTwo}.

\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{FederatedScheme1}
\caption{Schematic of filter streams on the y-axis over time on the x-axis. Measurements form electromagnetic (EM) tracking arrive more frequent and are taken from three sensors. Therefore the local filter for EM tracking can always perform a Kalman update step, where the Kalman prediction is weighted against the incoming measurement. Optical tracking above may have a higher static accuracy, but due to the slower sampling rate, every second estimate is just based on the Kalman prediction denoted by an encircled P. Using a scalable filter scheme, the output frequency can be chosen freely, e.g.\ at 25\,Hz to match it to a certain display update rate.}
\label{fig:FederatedOne}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{FederatedScheme2}
\caption{Schematic of the filtering with axes as above; after filtering individual streams, the master filter fuses all state vectors based on their respective covariance matrix.}
\label{fig:FederatedTwo}
\end{figure}

\subsection{Federated scheme: Master Filter}
The task of really fusing data streams based on a probabilistic model is then performed in the master filter.
It can be freely implemented in whatever filter form desired.
Instead of a second-stage Kalman Filter which would smoothen the final output some more, we decided to stay as close to the individual outputs as possible, therefore keeping a simple and efficient filter design.
This is achieved by building a weighted average out of the state updates and their respective covariances.
After the weighting, a normalization is necessary to keep the original dimensions of the state-vector, see equation~\eqref{eqn:MasterFilter}.
\begin{equation}
\bar{y}(t)=\frac{\sum_i P^{-1}_i(t)\cdot y_i(t)}{\sum_i P^{-1}_i(t)}  ,
\label{eqn:MasterFilter}
\end{equation}
where $y_i$ are the state-outputs of individual local filters and $P_i$ the respective covariances, $\bar{y}$ is the fused state and $t$ denotes time dependency.

As mentioned in section~\ref{sec:imms}, combining state-vectors in a linear fashion has to be performed with care.
Not all mathematical or physical quantities can be added.
Quaternions for example must be multiplied in order to express a new rotation $q_3$ that consists of two partial rotations $q_1, q_2$.
Here, $q_3 \not= q_1 + q_2$, but instead $q_3 = q_2\cdot q_1$ must be performed.
With quaternions also comes the problem that their multiplication is not commutative, so special care has to be taken from which side one applies the additional rotation.
In the current implementation the state vectors contain orientation information through three elements, the Euler angles $\left(\phi,\chi,\psi\right)$.
It was desired to keep them inbetween allowed rotation angles of $\pm\pi$ for $\phi$ and $\psi$ and $\pm\dfrac{\pi}{2}$ for $\chi$.

Weighting by the inverse of covariance matrices basically stands for an interpolation between two rotations.
The interpolation is only to be evaluated at one point, that is closer to the orientation which had the highest weight, i.e.\ lowest covariance i.e.\ lowest uncertainty.
Instead of just adding two weighted Euler vectors and then dividing by the sum of those weights as in equation~\eqref{eqn:MasterFilter}, a spherical linear interpolation is employed.
This technique avoids to increase the vector elements over allowed angle values for $\phi,\chi$ and $\psi$ by changing into rotation-matrix or quaternion space for the interpolation.
The interpolated result is then converted back into Euler representation to be assigned to the fused output vector.
%%% ===========================
%\section{Radioactive Tracking}
%\label{sec:gammatrack}
%\dots
%\label{sec:gammatrack_end}
%%% ===========================

\chapter{Results}
\label{ch:results}

The term 'error' will mostly relate to the mean of the absolute of the difference from estimate to ground truth, if not specified in a different manner.

\section{Artificial trajectories}
\label{sec:artData}

In order to account for a missing ground truth when fusing real tracking devices, several artificial trajectories were generated (see figure~\ref{fig:artTrajectories})

\begin{figure}
\centering
\begin{subfigure}[t]{0.45\textwidth}
\includegraphics[width=\textwidth]{artData_Line}
\caption{line data}
\end{subfigure}
~
\begin{subfigure}[t]{0.45\textwidth}
\includegraphics[width=\textwidth]{artData_Circle}
\caption{circle data}
\end{subfigure}

\begin{subfigure}[b]{0.5\textwidth}
\includegraphics[width=\textwidth]{artData_Eight}
\caption{eight data}
\end{subfigure}
\caption{(a) Line trajectory with constant speed and constant orientation, (b) Circle trajectory with constant speed but varying orientation, (c) Eight-shaped trajectory with rapidly changing orientation and also varying translational speed.}
\label{fig:artTrajectories}
\end{figure}
%\begin{table}[width=0.7\textwidth]
%\centering
%\caption{Results of 21 artificial trajectories filtered with the 'constant position' Kalman Filter. Sample frequencies of the EMT device and of the OT device were set to 20\,Hz.}
%\label{tab:constPosResults}
%\input{../ResultsConstPos.tex}
%\end{table}

\begin{table}[width=0.7\textwidth]
\centering
\caption{Results of 21 artificial trajectories filtered with the 'constant position' Kalman Filter. Sample frequency of the EMT device was set to 40\,Hz, of the OT device to 20\,Hz.}
\label{tab:constPosResults}
\input{../tables/ResultsConstPosEMT40Hz.tex}
\end{table}

%\begin{table}[width=0.7\textwidth]
%\centering
%\caption{Results of 21 artificial trajectories filtered with the 'constant velocity' Kalman Filter. Sample frequencies of the EMT device and of the OT device were set to 20\,Hz.}
%\label{tab:constPosResults}
%\input{../ResultsConstVel.tex}
%\end{table}
%
\begin{table}[width=0.7\textwidth]
\centering
\caption{Results of 21 artificial trajectories filtered with the 'constant velocity' Kalman Filter. Sample frequency of the EMT device was set to 40\,Hz, of the OT device to 20\,Hz.}
\label{tab:constVelResults}
\input{../tables/ResultsConstVelEMT40Hz.tex}
\end{table}
%
%\begin{table}[width=0.7\textwidth]
%\centering
%\caption{Results of 21 artificial trajectories filtered with the 'constant acceleration' Kalman Filter. Sample frequencies of the EMT device and of the OT device were set to 20\,Hz.}
%\label{tab:constPosResults}
%\input{../ResultsConstAcc.tex}
%\end{table}
%
\begin{table}[width=0.7\textwidth]
\centering
\caption{Results of 21 artificial trajectories filtered with the 'constant acceleration' Kalman Filter. Sample frequency of the EMT device was set to 40\,Hz, of the OT device to 20\,Hz.}
\label{tab:constAccResults}
\input{../tables/ResultsConstAccEMT40Hz.tex}
\end{table}

As we can see bla bla bla

\section{Interacting Multiple Model}
\subsection{Model probabilities}
Artificial data were also used to validate the correct function of the implemented IMM filter.
After applying the filter on an input trajectory, the course of model probabilities $\mu^i$ can be visualized.
The three formerly shown filter models 'constant position', 'constant velocity' and 'constant acceleration' were run as interacting models and applied on a trajectory especially diesigned for the three filters.
First, the OT and EMT simulate a static position, which then goes into n accelerated motion and then stays at a certain speed.
To simplify the conditions and make the result more representative for a best case scenario, motion was only carried out in one constant direction and no rotation was performed during motion.
As in the other artificial trajectories, 6D additional noise was applied on the translational and the rotational part as well.
Figure~\ref{fig:IMMExample} shows the model probabilities switching to whatever filter type seemed to predict future measurements the best.

\begin{figure}[htb]
\centering
\begin{subfigure}[t]{0.7\textwidth}
\includegraphics[width=\textwidth]{IMMprobsEMT20Hz_EMT}
\end{subfigure}
\\
\begin{subfigure}[b]{0.7\textwidth}
\includegraphics[width=\textwidth]{IMMprobsEMT20Hz_OT}
\end{subfigure}
\caption{Model probabilities $\mu_k^i$ on the y-axis versus sample index on the x-axis. The switching matrix $H_{ij}$ and the quality of each model's prediction govern the development of $\mu_k^i$. The upper image shows $\mu_{k,\mathrm{EMT}}^i$ for simulated electromagnetic tracking at 20\,Hz, while the lower image shows $\mu_{k,\mathrm{OT}}^i$ for optical tracking at 20\,Hz, which has a lower additive noise and therefore slightly different curves.}
\label{fig:IMMExample}
\end{figure}

\begin{figure}[htb]
\centering
\begin{subfigure}[b]{0.45\textwidth}
\includegraphics[width=\textwidth]{IMMPosition}
\caption{position}
\end{subfigure}
~
\begin{subfigure}[b]{0.45\textwidth}
\includegraphics[width=\textwidth]{IMMSpeed}
\caption{speed}
\end{subfigure}
\caption{(a) Position in x-direction of the trajectory used for figure~\ref{fig:IMMExample} with units in millimeters on the y-axis versus sample index on the x-axis, (b) Speed in per cent, the static phase, acceleration phase and constant speed phase are clearly separable.}
\label{fig:IMMExampleSpeeds}
\end{figure}

In order to get a better image of what the simulated trajectory was doing, figure~\ref{fig:IMMExampleSpeeds} shows the position and speed of the estimated tracker.
Due to the constant acceleration part in the middle, position goes up in a parabolic fashion and then keeps rising in a linear fashion during the constant velocity part in the third part.

\subsection{Difference to ground truth}

When applying the IMM filter on the same trajectories as in section~\ref{sec:artData}, its performance is in the same order of magnitude, but shows some important differences (see tables~\ref{tab:IMMResults20Hz} and~\ref{tab:IMMResults40Hz}).
A thing to note up front is that the used IMM filter was implemented in a fashion of linearly combining output states of the single models.
This was also done for the Euler angle part, which results in huge errors naturally when dealing with high changes in orientation as in the trajectories depicting an eight.
So in order to compare the IMM performance to the single local filter types' performance, a focus on the translational error is necessary.
An IMM filter dealing with Euler angles in an appropriate fashion is tedious to do but desired for future work (see section~\ref{subsec:OutlookIMM}).

\begin{table}[width=0.7\textwidth]
\centering
\caption{Results of 21 artificial trajectories filtered with the 'Interacting Multiple Model' Kalman Filter. Sample frequency of the EMT device was set to 40\,Hz, of the OT device to 20\,Hz.}
\label{tab:IMMResults40Hz}
\input{../tables/ResultsIMMEMT40Hz.tex}
\end{table}
\begin{table}[width=0.7\textwidth]
\centering
\caption{Results of 21 artificial trajectories filtered with the 'Interacting Multiple Model' Kalman Filter. Sample frequencies of the EMT device and of the OT device were set to 20\,Hz.}
\label{tab:IMMResults20Hz}
\input{../tables/ResultsIMM.tex}
\end{table}

In the translational error, the IMM filter has a mean error below the limit of 0.6\,mm even in cases of 100\mms~speed of motion.
This is only reached when the trajectory described a constant line motion, but curiously the error there is as low as 0.2\,mm at maximal speed, while the 'constant velocity'-filter had 0.93\,mm error.

While the IMM filter performs better if both modalities sample with the same frequency of 20\,Hz, the single local filter types always perform better when the EMT modality is sampled at 40\,Hz.

\section{Static precision}
When testing different speeds and accelerations in the evaluation of the IMM filter, some instability was observed in separating states of constant velocity and constant acceleration.
The full measurements are not listed here but examples for those instabilities are given in the appendix.\comment{make 3 screenshots for bad IMM model probabilities in appendix, take those with a = 10, 100 and 500}
The static state on the other hand was detected very reliable in less than a second.

Therefore the effect of applying an IMM filter that switched to 'constant position' mode on static data with additional noise was evaluated.
The results are shown in table~\ref{tab:staticFilter} and Gaussian distributions on the position are shown in figure~\ref{fig:staticFilterHists}.

\section{Calibration results}
For the tracking tool used for EMT and OT tracking, several calibrations were done in order to test visualization, fusion and naturally also the calibration itself.
Results of those calibrations are shown here.
The quality measure is a norm of the residual translational error $b_{res}$ over all used motions (see page~\pageref{eqn:linear}).

\section{Radioactive Tracking results}
With many many tracking technologies on the market but still not being able to track an object inside the human body without distortion and without the necessity of a line-of-sight, a first attempt on radioactive tracking was done.
A radioactive probe with about 1\,MBq activity was scanned with a hand-held gamma camera.
During the scan, the camera was moved back and forth over the probe, moved by a robotic arm which was tracked via optical tracking (NDI Polaris and NDI Spectra).
The effectiveness of the camera in terms of photon sensitivity should be evaluated in motion.

On the recorded data, a Gaussian fit was applied in order to find the center of the probe.
This approach is compared to a maximum intensity segmentation, as the probe should coiincide with the brightest spot on the gamma camera image.
Screenshots of the radiation images are shown in figures~\ref{fig:GammaCam1} to~\ref{fig:GammaCam5}.
Evaluation of the segmented centerpoint is shown in table~\ref{tab:GammaCamCenter}.